package datastructure;

import cellular.CellState;

import java.util.Arrays;

public class CellGrid implements IGrid {
    private final int rows;
    private final int columns;
    private final CellState initialState;
    private final CellState[][] grid;

    public CellGrid(int rows, int columns, CellState initialState) {
        this.rows = rows;
        this.columns = columns;
        this.initialState = initialState;
        this.grid = new CellState[rows][columns];
        for(CellState[] row : grid) {
            Arrays.fill(row, initialState);
        }
    }

    @Override
    public int numRows() {
        return rows;
    }

    @Override
    public int numColumns() {
        return columns;
    }

    /**
     * Checks that the row index and the column index are inbound.
     *
     * @param row    The row of the cell to get contents of
     * @param column The column of the cell to get the contents of
     * @return boolean value
     */
    private boolean inbounds(int row, int column) {
        boolean rowInbound = 0 <= row && row <= numRows() - 1;
        boolean columnInbound = 0 <= column && column <= numColumns() - 1;
        return rowInbound && columnInbound;
    }

    @Override
    public void set(int row, int column, CellState element) {
        if (inbounds(row, column)) {
            grid[row][column] = element;
        }else {
            throw new IndexOutOfBoundsException("out of bounds");
        }
    }

    @Override
    public CellState get(int row, int column) {
        if (inbounds(row, column)) {
            return grid[row][column];
        }else {
            throw new IndexOutOfBoundsException("out of bounds");
        }
    }

    @Override
    public IGrid copy() {
        CellGrid cellGridCopy = new CellGrid(rows, columns, initialState);
        for (int row = 0; row < rows; row++) {
            for (int column = 0; column < columns; column++) {
                cellGridCopy.set(row, column, get(row, column));
            }
        }
        return cellGridCopy;
    }
    
}
